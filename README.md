# aurora-obidian-notes



## Como Navegar?
Clicar na pasta como na imagem e navegar entre os arquivos e pastas internos.
![alt text for screen readers](https://gitlab.com/a-taverna-dos-mestes/aurora-obidian-notes/-/raw/main/PrintsReadMe/FolderStructure1.png)

## Como baixar e utilizar as notas

### Web Download
#### Requerimentos
- Obsidian: [Download Obsidian](https://obsidian.md/download)

##### Passo a Passo
Nessa página, clique no botão de download\
![alt text for screen readers](https://gitlab.com/a-taverna-dos-mestes/aurora-obidian-notes/-/raw/main/PrintsReadMe/DownloadButton.png)\
extraia o zip para uma pasta\
![alt text for screen readers](https://gitlab.com/a-taverna-dos-mestes/aurora-obidian-notes/-/raw/main/PrintsReadMe/ExtractZipContent.png)\
Abra o Obsidian após instalado e no lado esquerdo em baixo tem um menu para acesso, selecione o item marcado com a seta\
![alt text for screen readers](https://gitlab.com/a-taverna-dos-mestes/aurora-obidian-notes/-/raw/main/PrintsReadMe/OnObsidialLowerLeftSide.png)\
No menu aberto selecione a opção de Selecionar pasta como cofre\
![alt text for screen readers](https://gitlab.com/a-taverna-dos-mestes/aurora-obidian-notes/-/raw/main/PrintsReadMe/SelectOpenFolderAsVault.png)\
Navegue até sua pasta, selecione e abra\
![alt text for screen readers](https://gitlab.com/a-taverna-dos-mestes/aurora-obidian-notes/-/raw/main/PrintsReadMe/NavigateAndSelectYourExtractedFolder.png)\


### Git
#### Requerimentos
- Git: [Baixar e instalar](https://git-scm.com/downloads)
- Obsidian: [Download Obsidian](https://obsidian.md/download)