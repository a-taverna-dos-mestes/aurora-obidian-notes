### Luthién

#### Informações Gerais
- Tipo: #npc
---
 - Irmãos: 
 - Pai:  
 - Mãe: 
 - Esposa: [[Sísifo Zoldyck]]
 - Filhos: [[Manigold Zoldyck]] , [[Lerissa]]
---
 - Classe: 
 - Raça: Elfo (Alto)
 - Nível Aparente: 
 ---
 - Aliases e Codenomes: Regicida;
---
##### Tags
 - #npc

---
##### No Período deAmanhecer do Inferno
- Esposa do Lider da Mansão do Clã de Assasinos ao leste de Baldurs Gate

##### No Periodo yy (Anoitecer dos Deuses)
- 
 
##### No Periodo de Dois Nascimentos, uma Vida => One Shot
- Aparece grávida ao Final da Sessão, tem gêmeos. Porém um ritual é realizado e somente um bebê sai desse ritual, chamada de aberração [[Lerissa]] é levada aos clérigos, que não a querem levar também. [[Ekernon]] mata os outros clérigos e leva Lerissa para cuidar.
 