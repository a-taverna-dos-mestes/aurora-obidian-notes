### Baldurs Gate
#### Informações Gerais
##### Tags
 - #location
 - #city
 - #history_item

---
##### No Período xx (Amanhecer do Inferno)
- 

##### No Periodo yy (Anoitecer dos Deuses)
 - Participa do grupo formado no começo da campanha [[GrupoAnoitecerDosDeuses]]
 
##### No Periodo de Dois Nascimentos, Uma vida  => One Shot
 - [[Ekernon]]
	 - Clérigo que Vivia em **Baldur's Gate**
 - Guardada fortemente por um exército chamado de [[Flaming Fists]] (Punhos Flamejantes), e seu lider [[Ulder Ravengard]]
