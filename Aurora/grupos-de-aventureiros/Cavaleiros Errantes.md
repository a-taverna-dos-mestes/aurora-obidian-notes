### Cavaleiros Errantes
#### Informações Gerais
- Tipo: #grupo_aventureiros
-  Mesa:
	- [[Reino em Desespero]]
---
 - ##### Integrantes Atuais: 
	 - [[Altair]]
	 - [[Aldebaran]]
	 - [[Asuna]]
	 - [[Driel]]
	 - [[Gottlich]]
---
 - ##### Integrantes Originais
	- [[Altair]]
	- [[Aldebaran]]
	- [[Asuna]]
	- [[Driel]]
	- [[Gottlich]]
---
##### Tags
 - #grupo_aventureiros

---

##### No Período xx (Amanhecer do Inferno)
##### No Periodo yy (Anoitecer dos Deuses)
##### No Periodo zz (Anoitecer dos Deuses)(One Shot)