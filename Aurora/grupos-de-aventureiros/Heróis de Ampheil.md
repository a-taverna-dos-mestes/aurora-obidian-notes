### Heróis de Ampheil
#### Informações Gerais
- Tipo: #grupo_aventureiros
-  Mesa:
	- [[Amanhecer do Inferno]]
---
 - ##### Integrantes Atuais: 
	  - Burzo
	 - [[Nissia Ravengard]]
	 - [[Cari Naghere Nui]]
	 - [[Rúbeo Naghere Nui]]
	 - [[Manigold Zoldyck]]
---
 - ##### Integrantes Originais
	 - [[Aiz]]
	 - [[Burzo]]
	 - [[Nissia Ravengard]]
	 - [[Cari Naghere Nui]]
	 - [[Connhall de Astrid]]
	 - [[Rúbeo Naghere Nui]]
---
##### Tags
 - #grupo_aventureiros

---

##### No Período xx (Amanhecer do Inferno)
- [[Lerissa]]

##### No Periodo yy (Anoitecer dos Deuses)
- 

##### No Periodo zz (Anoitecer dos Deuses)(One Shot)
- 